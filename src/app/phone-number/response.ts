export interface Response {
  phoneNumbers: PhoneNumber[];
  size: number;
}

export interface PhoneNumber {
  countryName: string;
  state: boolean;
  countryCode: string;
  phoneNumber: string;
}
