import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {Response} from "./response";


@Injectable({
  providedIn: 'root'
})

export class PhoneNumberService {

  private endPoint = environment.apiBaseUrl;

  constructor(private httpClient: HttpClient) {
  }

  /**
   *
   * @param {string} state
   * @return  {Observable<PhoneNumber[]>}
   */
  public getPhoneNumbers(state: string): Observable<Response> {
    return this.httpClient.get<Response>(`${this.endPoint}/phone-numbers?state=${state}`)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  /**
   *
   * @param {string} countryCode
   * @param {string} state
   * @return  {Observable<PhoneNumber[]>}
   */
  public getPhoneNumbersByCountryCode(countryCode: string, state: string): Observable<Response> {
    return this.httpClient.get<Response>(`${this.endPoint}/phone-numbers/${countryCode}?state=${state}`)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  private httpError(error: { error: { message: string; }; status: any; message: any; }) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      msg = error.error.message;
    } else {
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

}
