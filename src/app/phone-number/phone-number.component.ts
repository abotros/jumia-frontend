import {Component, Input, OnInit} from '@angular/core';
import {PhoneNumberService} from "./phone-number.service";
import {PhoneNumber, Response} from "./response";

@Component({
  selector: 'app-phone-number',
  templateUrl: './phone-number.component.html',
})
export class PhoneNumberComponent implements OnInit {
  phoneNumbers: PhoneNumber[] = [];
  countryCode: string = '';
  state: string = '';

  @Input() page = 1;
  @Input() pageSize = 5;
  @Input() collectionSize = 1;

  constructor(public phoneNumberService: PhoneNumberService) {
  }

  ngOnInit() {
    this.fetchPhoneNumbers(this.state);
  }

  /**
   * @param event
   */
  public filterByCountry(event: any): void {
    this.countryCode = event.target.value;
    this.fetchPhoneNumberByCountryCode(this.countryCode, this.state);
  }

  /**
   * @param event
   * @return {void}
   */
  public filterByState(event: any): void {
    this.state = event.target.value;
    this.fetchPhoneNumberByCountryCode(this.countryCode, this.state)
  }

  /**
   * @param {string} state
   * @return {void}
   */
  private fetchPhoneNumbers(state: string): void {
    this.phoneNumberService.getPhoneNumbers(state).subscribe((data: Response) => {
      this.phoneNumbers = data.phoneNumbers;
      this.collectionSize = data.size
    })
  }

  /**
   * @param {string} countryCode
   * @param {string} state
   * @return {void}
   */
  private fetchPhoneNumberByCountryCode(countryCode: string, state: string): void {
    this.phoneNumberService.getPhoneNumbersByCountryCode(countryCode, state).subscribe((data: Response) => {
      this.phoneNumbers = data.phoneNumbers;
      this.collectionSize = data.size;
    })
  }
}
