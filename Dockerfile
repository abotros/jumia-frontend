# Build
FROM node:latest as build
WORKDIR /front-end
COPY ./ /front-end/
RUN npm install
RUN npm run build

# Package
FROM nginx:latest
COPY --from=build /front-end/dist/front-end /usr/share/nginx/html
